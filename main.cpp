#ifndef __PROGTEST__
#include <cassert>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>
#include <vector>
#include <map>
#include <set>
#include <list>
#include <algorithm>
#include <memory>
#include <functional>
using namespace std;

class CTimeStamp
{
  public:
                    CTimeStamp()= default;
                   CTimeStamp                              ( int               year,
                                                             int               month,
                                                             int               day,
                                                             int               hour,
                                                             int               minute,
                                                             double            sec ):year(year),month(month),day(day),hour(hour),
                                                             minute(minute),sec(sec){

                       time =(this->sec*1000)+
                          (this->minute*60000)+
                          (this->hour*3600000)+
                          (this->day*86400000)+
                          (this->month*2592000000)+
                          (this->year*31104000000);
                   }
                    CTimeStamp& operator= (const CTimeStamp & ts){year=(ts.year);month=(ts.month);
                   day=(ts.day);hour=(ts.hour);
                   minute=(ts.minute);sec=(ts.sec);
                    return *this;
                   }


    int            Compare                                 ( const CTimeStamp & x ) const{
                      if(time>x.time){
                          return 1;

                      }else if(time==x.time){
                          return 0;
                      }
                      else return -1;
                   }
    friend ostream & operator <<                           ( ostream          & os,
                                                             const CTimeStamp & x ){
                       os<<x.year<<"-"<<setfill('0')<<setw(2)<<x.month<<"-"<<setfill('0')<<setw(2)<<x.day<<" "
                               <<setfill('0')<<setw(2)<<x.hour<<":"<<setfill('0')<<setw(2)<<x.minute<<":"<<fixed<<setprecision(3)<<setw(5)<<x.sec;
                       return os;
                   }
  private:

    int               year=0;
    int               month=0;
    int               day=0;
    int               hour=0;
    int               minute=0;
    double            sec=0;

     unsigned long long  int time;
};
class CMail
{
  public:


    CMail                                   ( const CTimeStamp & timeStamp,
                                                             const string     & from,
                                                             const string     & to,
                                                             const string     & subject ){
        _subject=subject;
        _timeStamp=timeStamp;
        _from=from;
        _to=to;
        _subject=subject;
    }
    int            CompareByTime                           ( const CTimeStamp & x ) const{
        return _timeStamp.Compare(x);
    }
    int            CompareByTime                           ( const CMail      & x ) const{
        return _timeStamp.Compare(x._timeStamp);
    }
    const string & From                                    ( void ) const{
        return _from;
    }
    const string & To                                      ( void ) const{
        return _to;
    }
    const string & Subject                                 ( void ) const{
        return _subject;
    }
    const CTimeStamp & TimeStamp                           ( void ) const{
        return _timeStamp;
    }
    friend ostream & operator <<                           ( ostream          & os,
                                                             const CMail      & x ){
        os<<x._timeStamp<<" "<<x._from<<" -> "<<x._to<<", subject: "<<x._subject;
        return os;
    }
  private:
     CTimeStamp  _timeStamp;
     string      _from;
     string      _to;
     string      _subject;
};
// your code will be compiled in a separate namespace
namespace MysteriousNamespace {
#endif /* __PROGTEST__ */
//----------------------------------------------------------------------------------------
enum send{
    FROM,SUBJECT,TO
};
class TempMail{
public:
    explicit TempMail(const string &from):_from(from){

    }
    string getFrom() const {
        return _from;
    }
    string getSubject() const {
        return _subject;
    }
    void setSubject(const string &subject) {
        TempMail::_subject = subject;
    }

private:

    string _from="";
    string _subject="";

};
class bonusTS{
public:
    bonusTS(const CTimeStamp & timeStamp): _timeStamp(timeStamp){

    }
    bool operator<(const bonusTS & timeStamp)const {
        int x= _timeStamp.Compare(timeStamp._timeStamp);
        return x < 0;
    }
    bool operator<(const CTimeStamp & timeStamp)const {
        int x= _timeStamp.Compare(timeStamp);
        return x < 0;
    }
private:
    CTimeStamp _timeStamp;
};
class CMailLog
{
  public:
    CMailLog()= default;
    int            ParseLog                                ( istream          & in ){
        string s;
        int month,day,year,hour,minute;
        char dou[3];
        double second;
        string relay_name,mailID,message,partMessage;
        int messagePart;
        int numberOfMails=0;
        while (!in.eof()) {
            //get one line data
            in >> s;
            month=monthConvert(s);
            in >>day>>year>>hour>>dou[0]>>minute>>dou[1]>>second>>relay_name>>mailID;
            in>>message;
            getline(in,partMessage);
            message+=partMessage;
            //---------------------------
            messagePart=decodeMessage(message);
            if(in.eof()){
                break;
            }
            switch(messagePart){
                case FROM:{
                    TempMail newMail(message.substr(5,message.size()-1));
                    _tempMails.insert(pair<string,TempMail>(mailID,newMail));
                    break;
                }case SUBJECT:{
                    auto mail=_tempMails.find(mailID);
                    if(mail == _tempMails.end()){
                        break;
                    }
                    mail->second.setSubject(message.substr(8,message.size()-1));
                    break;
                }case TO:{
                    auto mail=_tempMails.find(mailID);
                    if(mail == _tempMails.end()){
                        break;
                    }
                    CTimeStamp newTimestamp(year,month,day,hour,minute,second);
                    CMail newMail(newTimestamp,mail->second.getFrom(),message.substr(3,message.size()-1),mail->second.getSubject());

                    bonusTS tmpTimestamp(newTimestamp);
                    _mailMap.insert(pair<bonusTS ,CMail>(tmpTimestamp,newMail));
                    numberOfMails++;
                    break;
                }
                default:{

                }
            }


        }
        return numberOfMails;
    }

    list<CMail>    ListMail                                ( const CTimeStamp & from,
                                                             const CTimeStamp & to ) const{
        bonusTS fromTS(from);
        bonusTS toTS(to);
        list<CMail> newList;

        auto itlow=_mailMap.lower_bound (fromTS);
        auto itup=_mailMap.upper_bound (toTS);

        for(;itlow!=itup;itlow++){
            newList.push_back(itlow->second);
        }

        return newList;
    }

    set<string>    ActiveUsers                             ( const CTimeStamp & from,
                                                             const CTimeStamp & to ) const{
        bonusTS fromTS(from);
        bonusTS toTS(to);
        set<string> newSet;

        auto itlow=_mailMap.lower_bound (fromTS);
        auto itup=_mailMap.upper_bound (toTS);

        for(;itlow!=itup;itlow++){
            newSet.insert(itlow->second.From());
            newSet.insert(itlow->second.To());
        }

        return newSet;
    }
  private:
    int decodeMessage(const string & message){
        switch(message[0]){
            case 'f':{
                if(message.substr(0,5)=="from="){
                    return FROM;
                }else{
                    return -1;
                }
            }case 's':{
                if(message.substr(0,8)=="subject="){
                    return SUBJECT;
                }else{
                    return -1;
                }
            }case 't':{
                if(message.substr(0,3)=="to="){
                    return TO;
                }else{
                    return -1;
                }
            }
            default:{
                return -1;
            }
        }
    }
    int monthConvert(const string& s){
        if(s=="Jan"){
            return 1;
        }else if(s=="Feb"){
            return 2;
        }else if(s=="Mar"){
            return 3;
        }else if(s=="Apr"){
            return 4;
        }else if(s=="May"){
            return 5;
        }else if(s=="Jun"){
            return 6;
        }else if(s=="Jul"){
            return 7;
        }else if(s=="Aug"){
            return 8;
        }else if(s=="Sep"){
            return 9;
        }else if(s=="Oct"){
            return 10;
        }else if(s=="Nov"){
            return 11;
        }else if(s=="Dec"){
            return 12;
        }else{
            return 0;
        }
    }

    map<string,TempMail> _tempMails;
    multimap<bonusTS ,CMail> _mailMap;

};
//----------------------------------------------------------------------------------------
#ifndef __PROGTEST__
} // namespace
string             printMail                               ( const list<CMail> & all )
{
  ostringstream oss;
  for ( const auto & mail : all )
    oss << mail << endl;
  return oss . str ();
}
string             printUsers                              ( const set<string> & all )
{
  ostringstream oss;
  bool first = true;
  for ( const auto & name : all )
  {
    if ( ! first )
      oss << ", ";
    else
      first = false;
    oss << name;
  }
  return oss . str ();
}
int                main                                    ( void )
{
  MysteriousNamespace::CMailLog m;
  list<CMail> mailList;
  set<string> users;
  istringstream iss;

  iss . clear ();
  iss . str (
    "Mar 29 2019 12:35:32.233 relay.fit.cvut.cz ADFger72343D: from=user1@fit.cvut.cz\n"
    "Mar 29 2019 12:37:16.234 relay.fit.cvut.cz JlMSRW4232Df: from=person3@fit.cvut.cz\n"
    "Mar 29 2019 12:55:13.023 relay.fit.cvut.cz JlMSRW4232Df: subject=New progtest homework!\n"
    "Mar 29 2019 13:38:45.043 relay.fit.cvut.cz Kbced342sdgA: from=office13@fit.cvut.cz\n"
    "Mar 29 2019 13:36:13.023 relay.fit.cvut.cz JlMSRW4232Df: to=user76@fit.cvut.cz\n"
    "Mar 29 2019 13:55:31.456 relay.fit.cvut.cz KhdfEjkl247D: from=PR-department@fit.cvut.cz\n"
    "Mar 29 2019 14:18:12.654 relay.fit.cvut.cz Kbced342sdgA: to=boss13@fit.cvut.cz\n"
    "Mar 29 2019 14:48:32.563 relay.fit.cvut.cz KhdfEjkl247D: subject=Business partner\n"
    "Mar 29 2019 14:58:32.000 relay.fit.cvut.cz KhdfEjkl247D: to=HR-department@fit.cvut.cz\n"
    "Mar 29 2019 14:25:23.233 relay.fit.cvut.cz ADFger72343D: mail undeliverable\n"
    "Mar 29 2019 15:02:34.231 relay.fit.cvut.cz KhdfEjkl247D: to=CIO@fit.cvut.cz\n"
    "Mar 29 2019 15:02:34.230 relay.fit.cvut.cz KhdfEjkl247D: to=CEO@fit.cvut.cz\n"
    "Mar 29 2019 15:02:34.230 relay.fit.cvut.cz KhdfEjkl247D: to=dean@fit.cvut.cz\n"
    "Mar 29 2019 15:02:34.230 relay.fit.cvut.cz KhdfEjkl247D: to=vice-dean@fit.cvut.cz\n"
    "Mar 29 2019 15:02:34.230 relay.fit.cvut.cz KhdfEjkl247D: to=archive@fit.cvut.cz\n" );
  assert ( m . ParseLog ( iss ) == 8 );
  mailList = m . ListMail ( CTimeStamp ( 2019, 3, 28, 0, 0, 0 ),
                            CTimeStamp ( 2019, 3, 29, 23, 59, 59 ) );

  assert ( printMail ( mailList ) ==
    "2019-03-29 13:36:13.023 person3@fit.cvut.cz -> user76@fit.cvut.cz, subject: New progtest homework!\n"
    "2019-03-29 14:18:12.654 office13@fit.cvut.cz -> boss13@fit.cvut.cz, subject: \n"
    "2019-03-29 14:58:32.000 PR-department@fit.cvut.cz -> HR-department@fit.cvut.cz, subject: Business partner\n"
    "2019-03-29 15:02:34.230 PR-department@fit.cvut.cz -> CEO@fit.cvut.cz, subject: Business partner\n"
    "2019-03-29 15:02:34.230 PR-department@fit.cvut.cz -> dean@fit.cvut.cz, subject: Business partner\n"
    "2019-03-29 15:02:34.230 PR-department@fit.cvut.cz -> vice-dean@fit.cvut.cz, subject: Business partner\n"
    "2019-03-29 15:02:34.230 PR-department@fit.cvut.cz -> archive@fit.cvut.cz, subject: Business partner\n"
    "2019-03-29 15:02:34.231 PR-department@fit.cvut.cz -> CIO@fit.cvut.cz, subject: Business partner\n" );
  mailList = m . ListMail ( CTimeStamp ( 2019, 3, 28, 0, 0, 0 ),
                            CTimeStamp ( 2019, 3, 29, 14, 58, 32 ) );
  assert ( printMail ( mailList ) ==
    "2019-03-29 13:36:13.023 person3@fit.cvut.cz -> user76@fit.cvut.cz, subject: New progtest homework!\n"
    "2019-03-29 14:18:12.654 office13@fit.cvut.cz -> boss13@fit.cvut.cz, subject: \n"
    "2019-03-29 14:58:32.000 PR-department@fit.cvut.cz -> HR-department@fit.cvut.cz, subject: Business partner\n" );
  mailList = m . ListMail ( CTimeStamp ( 2019, 3, 30, 0, 0, 0 ),
                            CTimeStamp ( 2019, 3, 30, 23, 59, 59 ) );
  assert ( printMail ( mailList ) == "" );
  users = m . ActiveUsers ( CTimeStamp ( 2019, 3, 28, 0, 0, 0 ),
                            CTimeStamp ( 2019, 3, 29, 23, 59, 59 ) );
  //cout<<printUsers ( users )<<endl;
  assert ( printUsers ( users ) == "CEO@fit.cvut.cz, CIO@fit.cvut.cz, HR-department@fit.cvut.cz, PR-department@fit.cvut.cz, archive@fit.cvut.cz, boss13@fit.cvut.cz, dean@fit.cvut.cz, office13@fit.cvut.cz, person3@fit.cvut.cz, user76@fit.cvut.cz, vice-dean@fit.cvut.cz" );
  users = m . ActiveUsers ( CTimeStamp ( 2019, 3, 28, 0, 0, 0 ),
                            CTimeStamp ( 2019, 3, 29, 13, 59, 59 ) );
  assert ( printUsers ( users ) == "person3@fit.cvut.cz, user76@fit.cvut.cz" );

  return 0;
}
#endif /* __PROGTEST__ */
